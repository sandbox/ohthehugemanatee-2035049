<?php

class openlayers_ranged_heatmaps extends openlayers_behavior {
  /**
   * Override of options_form().
   */
  function options_form($defaults = array()) {
 
    $options['fields'] = array(
      '#title' => t('Fields'),
      '#description' => t('Enter a comma separated list of attribute fields that can be used for scaling points. The first found attribute will be used.'),
      '#type' => 'textfield',
      '#default_value' => isset($defaults['fields']) ? $defaults['fields'] : array(),
    );
    $number = 16;
    $options['ranges'] = array(
      '#tree' => TRUE,
      '#title' => t('Color ranges'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    for ($i = 1; $i < $number; $i++) {
      $options['ranges'][$i] = array(
        '#title' => t('Range @i', array('@i' => $i)),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => !empty($defaults['ranges'][$i]['min']) || !empty($defaults['ranges'][$i]['max']) || !empty($defaults['ranges'][$i]['color']) ? FALSE : TRUE,
        '#tree' => TRUE,
      );
      $options['ranges'][$i]['min'] = array(
        '#title' => t('Minimum value'),
        '#type' => 'textfield',
        '#default_value' => isset($defaults['ranges'][$i]['min']) ? $defaults['ranges'][$i]['min'] : array(),
      );
      $options['ranges'][$i]['max'] = array(
        '#title' => t('Maximum value'),
        '#type' => 'textfield',
        '#default_value' => isset($defaults['ranges'][$i]['max']) ? $defaults['ranges'][$i]['max'] : array(),
      );
      $options['ranges'][$i]['color'] = array(
        '#type' => 'jquery_colorpicker',
        '#title' => t('Color'),
        '#default_value' => isset($defaults['ranges'][$i]['color']) ? $defaults['ranges'][$i]['color'] : 'FFFFFF',
      );
    }
    return $options;
  }

  /**
   * Generate an array of ranges and colors.
   * We can just take this from the behavior form.
   */
  function get_ranges() {
    $options = $this->options;
    foreach($options['ranges'] as $key => $value) {
      if (is_array($value)) {
        if (empty($value['min']) && empty($value['max']) && $value['color'] == 'FFFFFF') {
          break;
        }
        $ranges[] = $value;
      }
    }
    return $ranges;
  }

  /**
   * Retrieve the first found usable field from a set of features.
   */
  protected function get_field($features) {
    $fields = explode(',', $this->options['fields']);
    foreach ($fields as $k => $v) {
      $fields[$k] = trim($v);
    }
    foreach ($features as $feature) {
      foreach ($fields as $field) {
        if (isset($feature['attributes'][$field])) {
          return $field;
        }
      }
    }
    return FALSE;
  }

  /**
   * Get Vector layers.
   */
  protected function get_layers($layers) {
    $vector_layers = array();
    foreach ($layers as $key => $layer) {
      // get type == Vector for backwards-compatibility.
      // TODO: After OpenLayers alpha8 it should be removed
      if (((isset($layer['vector']) && $layer['vector'] == 1)) && !empty($layer['features'])) {
        $vector_layers[$key] = $layer;
      }
    }
    return $vector_layers;
  }

  /**
   * Render.
   */
  function render(&$map) {
    // Get the overlay layers which are Vector layers.
    $layers = $this->get_layers($map['layers']);
    // Get the ranges and colors assigned by the user.
    $ranges = $this->get_ranges();
    
    foreach ($layers as $k => $layer) {
      // Make sure this layer contains features with our field.
      if ($field = $this->get_field($layer['features'])) {
        foreach ($layer['features'] as $j => $feature) {
          // For each Feature on this layer, make sure the field has a value
          // (and give it a handy variable).
          if ($field && isset($feature['attributes'][$field]) && $value = $feature['attributes'][$field]) {
            // remove non-numeric characters (dollar signs etc) from the Value.
            $value = preg_replace("/[^0-9,.]/", "", $value);
            // Set a color value for the Feature based on which range it's in. 
            foreach ($ranges as $r => $range) {
              if ($value >= $range['min'] && $value <= $range['max']) {
                $string = "setting layer " . $k . ", feature " . $j . " color to " . $range['color'] . "for value of " . $value;
                $map['layers'][$k]['features'][$j]['attributes']['color'] = '#' . $range['color'];
                break;
              }
            }
          }
        }
      }
    }

    drupal_add_js(drupal_get_path('module', 'openlayers_ranged_heatmaps') . '/openlayers_ranged_heatmaps.js');
    return $this->options;
  }
}
