/**
 * Implementation of Drupal behavior.
 */
(function ($) {

Drupal.behaviors.openlayers_ranged_heatmaps = {
	    attach: function(context) {
  var data = $(context).data('openlayers');
  if (data && data.map.behaviors.openlayers_ranged_heatmaps) {
    var styles = data.map.behaviors.openlayers_ranged_heatmaps.styles;
    // Collect vector layers
    var vector_layers = [];
    for (var key in data.openlayers.layers) {
      var layer = data.openlayers.layers[key];
        for (var f in layer.features) {
          var feature = layer.features[f];
          console.log(feature);
          feature.style = OpenLayers.Util.applyDefaults({
            'fillColor': feature.attributes.color
          });
        }
        layer.redraw();
        vector_layers.push(layer);
    }
    /**
     * This attempts to fix a problem in IE7 in which points
     * are not displayed until the map is moved. 
     *
     * Since namespaces is filled neither on window.load nor
     * document.ready, and testing it is unsafe, this renders
     * map layers after 500 milliseconds.
     */
    if($.browser.msie) {
      setTimeout(function() {
        $.each(data.openlayers.getLayersByClass('OpenLayers.Layer.Vector'),
        function() {
          this.redraw();
        });
      }, 500);
    }
  }
	    }
};

})(jQuery);
